import java.lang.String;
public class GFG_4 { 
public static void main(String args[]) 
    { 
        String s = "geeksforgeeks"; 
  
        // toCharArray 
        char[] arr; 
        arr = s.toCharArray(); 
        System.out.println("String toCharArray: "); 
        for (char i : arr) 
            System.out.print(i + " "); 
  
        // getChars 
        s.getChars(5, 8, arr, 0); 
        System.out.println("\nSubString to existing "
                           + "char array"); 
        for (char i : arr) 
            System.out.print(i + " "); 
    } 
} 