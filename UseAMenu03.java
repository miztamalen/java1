// Import the required API classes.
import java.util.Scanner;
import java.lang.Character;
public class UseAMenu03
{
   public static void main(String[] args)
   {
      // Create the scanner.
      Scanner GetChoice = new Scanner(System.in);
      // Obtain input from the user.
      System.out.println("Optionsn");
      System.out.println("A. Yellow");
      System.out.println("B. Orange");
      System.out.println("C. Greenn");
      System.out.print("Choose your favorite color: ");
      char Choice = GetChoice.findInLine(".").charAt(0);
      // Convert the input to uppercase.
      Choice = Character.toUpperCase(Choice);
      // Choose the right color based on a switch statement.
      switch (Choice)
      {
         case 'A':
            System.out.println("Your favorite color is Yellow!");
            //break;
         case 'B':
            System.out.println("Your favorite color is Orange!");
            //break;
         case 'C':
            System.out.println("Your favorite color is Green!");
            //break;
         default:
            System.out.println(
                  "Type A, B, or C to select a color.");
            //break;
      }
   }
}