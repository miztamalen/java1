// Create the MyInt field.
private int MyInt = 0;
// Obtain the current value of MyInt.
public int getMyInt()
{
   return MyInt;
}
// Set a new value for MyInt.
public void setMyInt(int MyInt)
{
   this.MyInt = MyInt;
}