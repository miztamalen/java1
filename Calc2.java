import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Calc2 {
   private Map cache = new HashMap();
   public int square(int i) {
      int result = i * i;
      cache.put(i, result);
      return result;
   }
   public static void main(String[] args) throws Exception {
      Calc2 calc = new Calc2();
      Scanner input = new Scanner(System.in);
      
      while (true) {
         System.out.println("Enter a number between 1 and 100");
          int i = input.nextInt();
         System.out.println("Answer " + calc.square(i));
      }
      }
   }